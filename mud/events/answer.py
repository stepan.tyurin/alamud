# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event2, Event3

class AnswerWithEvent(Event2):
    NAME = "answer-with"

    def perform(self):
        self.inform("answer-with")
